//
// Created by jairman on 09.11.2022.
//

#include "image.h"


struct image* create_image (uint64_t width, uint64_t height) {
    struct image *img = malloc(sizeof (struct image));
    img->width = width;
    img->height = height;
    img->data = malloc(sizeof (struct pixel) * (width * height));
    return img;
}


void destroy_image(struct image* image) {
    free(image->data);
    free(image);
}


int64_t padding(size_t width){
    if(width % 4){
        return 4 - (int64_t)(width * sizeof(struct pixel) % 4);
    }
    return 0;
}
