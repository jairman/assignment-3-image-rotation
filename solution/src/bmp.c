//
// Created by jairman on 09.11.2022.
//

#include "bmp.h"


enum read_status from_bmp(FILE *in, struct image **image) {
    struct bmp_header header = {0};
    uint8_t status = fread(&header, sizeof(struct bmp_header), 1, in);
    if(!status){
        return READ_INVALID_HEADER;
    }
    *image = create_image(header.biWidth, header.biHeight);
    int64_t img_padding = padding(header.biWidth);
    uint64_t size = 0;
    for (int64_t i = 0; i < header.biHeight; i++) {
        for (int64_t j = 0; j < header.biWidth; j++) {
            status = fread(((*image)->data + size), sizeof (struct pixel), 1, in);
            if(!status){
                destroy_image(*image);
                return READ_INVALID_BITS;
            }
            size++;
        }
        status = fseek(in, img_padding, SEEK_CUR);
        if(status){
            destroy_image(*image);
            return READ_INVALID_SIGNATURE;
        }
    }
    return READ_OK;
}


enum write_status to_bmp( FILE* out, struct image const*  image){
    int64_t img_padding = padding(image->width);
    size_t file_size = image->width  * sizeof(struct pixel) + img_padding * image->height + sizeof(struct bmp_header);
    size_t image_size = (image->width + img_padding) * image->height;
    struct bmp_header header = (struct bmp_header) {
            .bfType = 0x4d42,
            .bfileSize = file_size,
            .bfReserved = 0,
            .bOffBits = 54,
            .biSize = 40,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = image_size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };
    uint8_t status = fwrite(&header, sizeof (struct bmp_header), 1, out);
    if(!status){
        return WRITE_HEADER_ERROR;
    }
    status = fseek(out, header.bOffBits, SEEK_SET);
    if(status){
        return WRITE_ERROR;
    }

    struct pixel *pix = image->data;
    for (int64_t i = 0; i < image->height; i++) {
        uint64_t size = sizeof(struct pixel) * image->width;
        struct pixel * target_str = image->data + i*image->width;
        status = fwrite(target_str, size, 1, out);
        if (!status) {
            return WRITE_ERROR;
        }
        status = fwrite(pix, img_padding, 1, out);
        if (img_padding != 0 && !status) {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}
