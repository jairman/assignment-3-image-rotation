#include "bmp.h"
#include "image.h"
#include "rotate.h"
#include <stdio.h>

int print_error(int status) {
    switch (status) {
        case READ_INVALID_ARGS:
            printf("%s", "Переданы неверные аргументы");
            return READ_INVALID_ARGS;
        case READ_ERROR:
            printf("%s", "Невозможно открыть файл");
            return READ_ERROR;
        case READ_INVALID_SIGNATURE:
            printf("%s", "READ_INVALID_SIGNATURE");
            return READ_INVALID_SIGNATURE;
        case READ_INVALID_BITS:
            printf("%s", "Неверные данные файла");
            return READ_INVALID_BITS;
        case READ_INVALID_HEADER:
            printf("%s", "Неверные заголовки файла");
            return READ_INVALID_HEADER;
        case WRITE_HEADER_ERROR:
            printf("%s", "Ошибка записи заголовков");
            return WRITE_HEADER_ERROR;
        case WRITE_ERROR:
            printf("%s", "Ошибка записи");
            return WRITE_ERROR;
        case CLOSE_ERROR:
            printf("%s", "Ошибка закрытия файла");
            return CLOSE_ERROR;
        default:
            printf("%s", "Неизвестная ошибка");
            return -1;

    }
}

int main(int argc, char** argv ) {
    if (argc != 3) {
        return print_error(READ_INVALID_ARGS);
    }
    FILE* read_file = fopen(argv[1], "rb");
    FILE* write_file = fopen(argv[2], "wb");
    if(read_file == NULL) {
        return print_error(READ_ERROR);
    }
    if(write_file == NULL) {
        return print_error(READ_ERROR);
    }
    struct image *image;
    enum read_status read_status = from_bmp(read_file, &image);
    if (read_status != READ_OK) {
        return print_error(read_status);
    }

    struct image *rotated = rotate(image);
    destroy_image(image);
    enum write_status write_status = to_bmp(write_file, rotated);
    if (write_status != WRITE_OK) {
        destroy_image(rotated);
        return print_error(write_status);
    }

    destroy_image(rotated);
    uint8_t status = fclose(read_file);
    if (status) {
        return print_error(CLOSE_ERROR);
    }

    return 0;
}
