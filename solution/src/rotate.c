//
// Created by jairman on 09.11.2022.
//

#include "rotate.h"
#include "image.h"


struct image* rotate(struct image* image) {
    struct image* rotated = create_image( image->height, image->width );
    for (int64_t i = 0; i < rotated->height; i++) {
        for (int64_t j = 0; j < rotated->width; j++) {
            rotated->data[i*rotated->width + j] = image->data[(image->height - 1 - j)*image->width + i];
        }
    }
    return rotated;
}
