//
// Created by jairman on 09.11.2022.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_IMAGEIO_H
#define ASSIGNMENT_3_IMAGE_ROTATION_IMAGEIO_H
#include "image.h"


enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE = 1,
    READ_INVALID_BITS = 2,
    READ_INVALID_HEADER = 3,
    READ_INVALID_ARGS = 4,
    READ_ERROR = 5
};

enum read_status from_bmp( FILE* in, struct image** image );

enum  write_status {
    WRITE_OK = 10,
    WRITE_HEADER_ERROR = 11,
    WRITE_ERROR = 12,
    CLOSE_ERROR = 13
};

enum write_status to_bmp( FILE* out, struct image const* image );


#endif //ASSIGNMENT_3_IMAGE_ROTATION_IMAGEIO_H
